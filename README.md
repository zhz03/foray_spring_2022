# FORAY_Spring_2022

## 0 Develop Branch

**Note:** Please checkout to `develop` branch before your develop anything

```shell
git checkout develp
```

## 1 Old resources

This list is sorted in Reverse Chronological Order: 

- [blimp repo in November 2021 competition](https://git.uclalemur.com/aaronjs/blimp)
- [November 2021 Blimp Competition](https://git.uclalemur.com/shahrulkamil98/november-2021-blimp-competition)
- [Visual processing PyTorch code of green balloon](https://git.uclalemur.com/aaronjs/foray-ball-detection/)
- [Blimp modelling git](https://github.com/zhz03/209_project_Blimp_modelling)
- [Blimp previous design git](https://git.uclalemur.com/ljhnick/foray)
- [Blimp joystick control git](https://github.com/zhz03/Tutorial-Controlling-multiple-control-boards-through-game-controllers)

